# Asset Tokenization

Blockchain project, Ethereum, Truffle, OpenZeppelin, ERC20, Web3.js, React, Unit Testing

The project is deployed to the Goerli test network and you can try it out on my page: https://mliviusz.github.io/
For validating account, you can inport the deployer account with the private key: c71d5c909c0eb45eaf757fb76fb8c5b3fa0b2f7a55a79543a39c5e9719a17510

# The Project

In this project, I made my own Fungable ERC20 Token with limited supply (1000000) where each token costs 1 wei. You can buy it with a bottom on my website or you can just transfer money to the address on the site, and you will automaticly receive your tokens.

# Development

During development I used Solidity, truffle, truffel-cli, web3, openzeppelin, React, metamask, hdwallet-provider, Infura
For unit testing I used trufle with mocha, chai, chai-as-promised


